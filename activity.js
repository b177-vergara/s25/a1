// 2

	db.fruits.aggregate([
  	{ $match: { onSale: true } },
  	{ $count: "total_fruits" }
	]);

// 3

	db.fruits.aggregate([
	  { $match: { stock: { $gt: 20 } } },
	  { $count: "total_fruits" }
	]);

// 4

	db.fruits.aggregate([ 
		{ $match: { onSale:true } },
		{ $group: { _id: "$supplier_id", average: { $avg: "$price" } } }
	]);
   
// 5

	db.fruits.aggregate([ 
		{ $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } }
  ]);

// 6

	db.fruits.aggregate([ 
		{ $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } }
	]);

